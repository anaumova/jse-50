package ru.tsc.anaumova.tm.api.service.dto;

import ru.tsc.anaumova.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.anaumova.tm.dto.model.AbstractModelDTO;

public interface IServiceDTO<M extends AbstractModelDTO> extends IRepositoryDTO<M> {
}