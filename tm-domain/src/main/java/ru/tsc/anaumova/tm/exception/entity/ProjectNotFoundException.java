package ru.tsc.anaumova.tm.exception.entity;

import ru.tsc.anaumova.tm.exception.AbstractException;

public final class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}